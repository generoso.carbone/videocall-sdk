package com.lifesensor.walltree;

public final class Const {
    //costanti per le custom action da utilizzare negli intent-filter
    public static final String ACTION_DECLINE = "eu.beamdigital.beamwatch.ACTION_DECLINE";
    public static final String ACTION_MISSED = "eu.beamdigital.beamwatch.ACTION_MISSED";

    public static final String TWILIO_ACCESS_TOKEN_SERVER = "https://powerful-anchorage-22116.herokuapp.com/";
}
