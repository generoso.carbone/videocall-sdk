package com.lifesensor.walltree;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.IOException;

public class IncomingCall extends AppCompatActivity {

    private Vibrator v;
    private MediaPlayer player;

    private String from;
    private String memberFrom;
    private String to;
    private String memberTo;
    private String room;
    private boolean callStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming_call);

        memberFrom = getIntent().getStringExtra("MemberFrom");
        from = getIntent().getStringExtra("From");
        memberTo = getIntent().getStringExtra("MemberTo");
        to = getIntent().getStringExtra("To");
        room = getIntent().getStringExtra("RoomId");

        if(memberFrom == null){
            throw new IllegalStateException("Missing parameter: MemberFrom");
        }

        if(from == null){
            throw new IllegalStateException("Missing parameter: From");
        }

        if(memberTo == null){
            throw new IllegalStateException("Missing parameter: MemberTo");
        }

        if(to == null){
            throw new IllegalStateException("Missing parameter: To");
        }

        if(room == null){
            throw new IllegalStateException("Missing parameter: RoomId");
        }


        try {
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            );

            v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        } catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e("WAKEVIBRATOR", e.getMessage());
        }
        long[] pattern = {0, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000};
        //long[] pattern = new long[]{0, 500, 110, 500, 110, 450, 110, 200, 110, 170, 40, 450, 110, 200, 110, 170, 40, 500};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                v.vibrate(VibrationEffect.createWaveform(pattern, 4));
            } catch (Exception e){
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("VIBRATOR", e.getMessage());
            }
        } else{
            try{
                if(v!=null)
                    v.vibrate(pattern, 4);
                else
                    Log.e("vibrate", "no vibrator");
            } catch (Exception e){
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("VIBRATOR", e.getMessage());
            }
        }

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            this.player = new MediaPlayer();
            this.player.setDataSource(this, notification);
            this.player.prepare();
            this.player.setVolume(1f, 1f);
            this.player.setLooping(true);
            this.player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }


        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("Thread", "callStarted: " + callStarted);
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.e("Thread", "callStarted: " + callStarted);
                if((!callStarted)){
                    try {
                        sendBroadcast(new Intent(Const.ACTION_MISSED));
                        stopRinging();
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Log.e("Thread", "started");
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(missedCall, new IntentFilter(Const.ACTION_MISSED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(missedCall);
    }

    public void answerCall(View view) {
        callStarted = true;
        try {
            v.cancel();
        }catch (Exception e){
            Log.e(getClass().getSimpleName().toUpperCase(), e.getMessage());
        }

        stopRinging();

        Intent i = new Intent(this, VideocallActivity.class);
        i.putExtra("MemberFrom", memberFrom);
        i.putExtra("From", from);
        i.putExtra("MemberTo", memberTo);
        i.putExtra("To", to);
        i.putExtra("RoomId", room);
        i.putExtra("wake-phone", true);
        i.putExtra("status", "called");
        startActivity(i);
        finish();
    }

    public void sendRejectedCall(View view) {
        Log.d("IncomingCall", "declined");
        //////////////////////////////////////////////
        /// HERE YOU SEND THE RejectedCall COMMAND ///
        //////////////////////////////////////////////
        finish();
    }

    BroadcastReceiver missedCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Toast.makeText(context, "Chiamata persa", Toast.LENGTH_SHORT).show();
            if(action==null)
                action="";
            if(action.equals(Const.ACTION_MISSED)) {
                Log.d("IncomingCall", "missed");

                finish();
            }
        }
    };

    @Override
    protected void onDestroy() {
        stopRinging();
        super.onDestroy();
    }

    private void stopRinging() {
        if(v!=null)
            v.cancel();

        try {
            if (this.player != null && this.player.isPlaying()) {
                this.player.stop();
                this.player.release();
            }
        } catch (Exception e){
            Log.e("stopRinging", "stop ringing: " + e.getMessage());
        }
    }
}